//
// Created by Li, Jixian on 2019-08-13.
//

#include "SimpTaskMap.h"

void SimpTaskMap::update(const SimplificationGraph &modGraph) {
  for (auto iter = modGraph.new_tids.begin(); iter != modGraph.new_tids.end(); ++iter){
    auto new_tid = iter->second;
    auto new_shard = modGraph.new_sids.at(new_tid);
    mShards[new_tid] = new_shard;
    mTasks[new_shard].push_back(new_tid);
  }
  mShards[modGraph.maxTid+1] = 0;
  mTasks[0].push_back(modGraph.maxTid+1);
}

BabelFlow::ShardId SimpTaskMap::shard(BabelFlow::TaskId id) const {
  auto iter = mShards.find(id);
  if (iter != mShards.end()) return iter->second;
  else return baseMap->shard(id);
}

std::vector<BabelFlow::TaskId> SimpTaskMap::tasks(BabelFlow::ShardId id) const {
  auto iter = mTasks.find(id);
  std::vector<BabelFlow::TaskId> modedTasks;
  if (iter != mTasks.end()) {
    modedTasks = iter->second;
  }
  std::vector<BabelFlow::TaskId> oldTasks = baseMap->tasks(id);
  oldTasks.insert(oldTasks.end(), modedTasks.begin(), modedTasks.end());
  return oldTasks;
}
