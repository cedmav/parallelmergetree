/*
 * KWayTaskMap.h
 *
 *  Created on: Dec 18, 2014
 *      Author: bremer5
 */

#ifndef KWAYTASKMAP_H_
#define KWAYTASKMAP_H_

//#include "TypeDefinitions.h"
#include "BabelFlow/TaskGraph.h"

#include "KWayMerge.h"

class KWayMerge;

class KWayTaskMap : public BabelFlow::TaskMap
{
public:

  //! Default constructor
  KWayTaskMap()=default; // added to accommodate ascent interface
  KWayTaskMap(BabelFlow::ShardId controller_count,const KWayMerge* task_graph);

  //! Destructor
  ~KWayTaskMap() {}

  //! Return which controller is assigned to the given task
  virtual BabelFlow::ShardId shard(BabelFlow::TaskId id) const;

  //! Return the set of task assigned to the given controller
  virtual std::vector<BabelFlow::TaskId> tasks(BabelFlow::ShardId id) const;

private:

  //! The number of controllers
  BabelFlow::ShardId mControllerCount;

  //! A reference to the task graph
  const KWayMerge* mTaskGraph;
};

#endif /* KWAYTASKMAP_H_ */
