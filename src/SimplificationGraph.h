//
// Created by Li, Jixian on 2019-08-12.
//

#ifndef PARALLELMERGETREE_SIMPLIFICATIONTREE_H
#define PARALLELMERGETREE_SIMPLIFICATIONTREE_H

#include "KWayMerge.h"
#include "BabelFlow/TaskGraph.h"

class SimplificationGraph : public BabelFlow::TaskGraph {
private:
  KWayMerge *baseGraph;

  std::vector<std::vector<BabelFlow::TaskId>> output_tasks;

public:
  std::map<BabelFlow::TaskId, BabelFlow::Task> old_tasks;
  std::map<uint64_t, BabelFlow::TaskId> old_g2t;
  std::map<BabelFlow::TaskId, BabelFlow::ShardId> new_sids;
  std::map<BabelFlow::TaskId, BabelFlow::TaskId> new_tids;
  std::map<BabelFlow::TaskId, uint64_t> new_gids;

  uint64_t maxGid = 0;
  BabelFlow::TaskId maxTid = 0;
  BabelFlow::CallbackId maxCallBackId = 0;

  SimplificationGraph(KWayMerge *baseGraph, KWayTaskMap *baseMap, BabelFlow::ShardId count);

  std::vector<BabelFlow::Task> localGraph(BabelFlow::ShardId id, const BabelFlow::TaskMap *task_map) const override;

  BabelFlow::Task task(uint64_t gId) const override;

  uint64_t gId(BabelFlow::TaskId tId) const override;

  BabelFlow::TaskId size() const override;

  BabelFlow::TaskId gid2otid(uint64_t gid) const {
    for (auto new_gid : new_gids) {
      if (new_gid.second == gid) {
        return new_gid.first;
      }
    }
    return BabelFlow::TNULL;
  }

  BabelFlow::Payload serialize() const override;

  void deserialize(BabelFlow::Payload payload) override;

  template<class K, class V>
  BabelFlow::Payload serializeMap(const std::map<K, V> &m) const
  {
    int32_t buffer_size = sizeof(size_t) + m.size() * sizeof(K) + m.size() * sizeof(V);
    char *buffer = new char[buffer_size];

    auto *count_ptr = reinterpret_cast<size_t *>(buffer);
    count_ptr[0] = m.size();

    auto *key_ptr = reinterpret_cast<K *>(buffer + sizeof(size_t));
    auto *value_ptr = reinterpret_cast<V *>(buffer + sizeof(size_t) + sizeof(K) * m.size());

    size_t count = 0;
    for (auto iter = m.begin(); iter != m.end(); ++iter) {
      key_ptr[count] = iter->first;
      value_ptr[count] = iter->second;
      count++;
    }
    return BabelFlow::Payload(buffer_size, buffer);
  }

  template<class K, class V>
  std::map<K, V> deserializeMap(char *buffer)
  {
    std::map<K, V> map_obj;
    size_t num_element = reinterpret_cast<size_t *>(buffer)[0];
    auto *key_ptr = reinterpret_cast<K *>(buffer + sizeof(size_t));
    auto *value_ptr = reinterpret_cast<V *>(buffer + sizeof(size_t) + sizeof(K) * num_element);
    for (size_t i = 0; i < num_element; ++i) {
      map_obj[key_ptr[i]] = value_ptr[i];
    }
    return map_obj;
  }
};

#endif //PARALLELMERGETREE_SIMPLIFICATIONTREE_H
