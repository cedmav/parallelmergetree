#include <string.h>

#include "SixSubdivisionNeighborhood.h"

SixSubdivisionNeighborhood::SixSubdivisionNeighborhood(LocalIndexType dim[3]) : Neighborhood(dim)
{
  int8_t neighbors[][3] = {
    {0, -1, -1}, // a
    {1, -1, -1}, // b
    {0, 0, -1}, // c
    {1, 0, -1}, // d
    {0, -1, 0}, // e
    {1, -1, 0}, // f
    {1, 0, 0}, // h
    {-1, 0, 1}, // diagonal g
    {0, 0, 1}, // diagonal h
    {-1, 0, 0}, // horizontal g
    {-1, 1, 0}, // body c
    {0, 1, 0}, // body d
    {-1, 1, 1}, // body g
    {0, 1, 1}, // body h
  };
  this->mCount = sizeof neighbors/sizeof neighbors[0];
  this->mNeighbors = new int8_t[3*this->mCount];
  this->mOffsets = new SignedLocalIndexType[this->mCount];
  memcpy(this->mNeighbors, neighbors, sizeof neighbors);

  this->computeOffsets();
}

SixSubdivisionNeighborhood::~SixSubdivisionNeighborhood()
{
  delete[] this->mNeighbors;
  delete[] this->mOffsets;
}



