#ifndef SIXSUBDIVISIONNEIGHBORHOOD_H_
#define SIXSUBDIVISIONNEIGHBORHOOD_H_

#include "Neighborhood.h"

class SixSubdivisionNeighborhood : public Neighborhood
{
public:

  SixSubdivisionNeighborhood(LocalIndexType dim[3]);

  ~SixSubdivisionNeighborhood();
};




#endif /* SIXSUBDIVISIONNEIGHBORHOOD_H_ */
