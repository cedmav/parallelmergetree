//
// Created by Li, Jixian on 2019-08-13.
//

#ifndef PARALLELMERGETREE_SIMPTASKMAP_H
#define PARALLELMERGETREE_SIMPTASKMAP_H

#include "BabelFlow/TaskGraph.h"
#include "KWayTaskMap.h"
#include "SimplificationGraph.h"

class SimpTaskMap : public BabelFlow::TaskMap {
public:
  KWayTaskMap *baseMap;
  std::map<BabelFlow::TaskId, BabelFlow::ShardId> mShards;
  std::map<BabelFlow::ShardId, std::vector<BabelFlow::TaskId>> mTasks;

  SimpTaskMap() = default;

  explicit SimpTaskMap(KWayTaskMap *_m) : baseMap(_m) {}

  void update(const SimplificationGraph &modGraph);

  BabelFlow::ShardId shard(BabelFlow::TaskId id) const override;

  std::vector<BabelFlow::TaskId> tasks(BabelFlow::ShardId id) const override;
};

#endif //PARALLELMERGETREE_SIMPTASKMAP_H
