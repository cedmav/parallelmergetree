//
// Created by Li, Jixian on 2019-08-12.
//

#include "SimplificationGraph.h"

// TODO DELETE THIS
#ifndef DEBUG_PRINT
#define  DEBUG_PRINT
#endif

#ifdef DEBUG_PRINT
#include "mpi.h"
#endif

using namespace BabelFlow;
using namespace std;

SimplificationGraph::SimplificationGraph(KWayMerge *_g, KWayTaskMap *_m, BabelFlow::ShardId count) {
#ifdef DEBUG_PRINT
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
#endif
  baseGraph = _g;
  output_tasks.resize(count);

  for (ShardId sid = 0; sid < count; ++sid) {
    vector<Task> local_tasks = baseGraph->localGraph(sid, _m);
    for (auto &&t:local_tasks) {
      TaskId tid = t.id();
      if (tid > maxTid) maxTid = tid;
      uint64_t gid = baseGraph->gId(tid);
      if (gid > maxGid) maxGid = gid;
      if (t.callback() > maxCallBackId) maxCallBackId = t.callback();

      if (t.outputs().empty()) {
        // this is a output task
        output_tasks[sid].push_back(tid);
        old_tasks[tid] = t;
        old_g2t[baseGraph->gId(tid)] = tid;
      }
    }
  }

  auto next_tid = maxTid + 1;
  auto next_gid = maxGid + 1;
  // reserve for simplification
  next_tid++;
  next_gid++;

  for (ShardId sid = 0; sid < count; ++sid) {
    for (auto &&otask: output_tasks[sid]) {
      new_sids[next_tid] = sid;
      new_tids[otask] = next_tid++;
      new_gids[otask] = next_gid++;
    }
  }
}

std::vector<BabelFlow::Task>
SimplificationGraph::localGraph(BabelFlow::ShardId id, const BabelFlow::TaskMap *task_map) const {
  // here we assume the taskmap is updated using SimplificationMap
  vector<TaskId> tids = task_map->tasks(id);
  vector<Task> tasks(tids.size());
  for (size_t i = 0; i < tids.size(); ++i) {
    tasks[i] = task(gId(tids[i]));
  }
  return tasks;
}

BabelFlow::Task SimplificationGraph::task(uint64_t gId) const {
  if (gId <= maxGid) {
    auto iter = old_g2t.find(gId);
    if (iter == old_g2t.end()) {
      return baseGraph->task(gId);
    } else {
      Task t = baseGraph->task(gId);
      t.outputs().resize(2);
      t.outputs()[0].push_back(maxTid + 1);
      t.outputs()[1].push_back(new_tids.at(iter->second));
      return t;
    }
  } else if (gId > maxGid + 1) {
    // update from global tree node
    auto old_tid = gid2otid(gId);
    auto new_tid = new_tids.at(old_tid);
    Task t(new_tid);

    t.callback(maxCallBackId+2);
    t.incoming().push_back(maxTid + 1);
    t.incoming().push_back(old_tid);
    t.outputs().resize(0);
    return t;
  } else {
    // simplification node
    Task t(maxTid + 1);
    // incoming from all old output node
    t.incoming().clear();
    t.outputs().resize(1);
    t.callback(maxCallBackId+1);
    for (auto &&tid_pair:new_tids) {
      t.incoming().push_back(tid_pair.first);
      t.outputs()[0].push_back(tid_pair.second);
    }
    return t;
  }
}

uint64_t SimplificationGraph::gId(BabelFlow::TaskId tId) const {
  if (tId <= maxTid) {
    return baseGraph->gId(tId);
  } else {
    return maxGid + (tId - maxTid);
  }
}

BabelFlow::TaskId SimplificationGraph::size() const {
  return baseGraph->size() + new_tids.size() + 1;
}

BabelFlow::Payload SimplificationGraph::serialize() const {
  Payload old = baseGraph->serialize();
  Payload p_tids = serializeMap<TaskId, TaskId>(new_tids);
  Payload p_gids = serializeMap<TaskId, uint64_t>(new_gids);
  Payload p_sids = serializeMap<TaskId, ShardId>(new_sids);
  Payload p_og2t = serializeMap<uint64_t, TaskId>(old_g2t);

  // merge all the payloads
  // header = |CallbackId|maxTid|maxGid|p_tids.offset|p_gids.offset|p_sids.offset|p_og2t.offset|old.offset|
  int32_t header_size = sizeof(CallbackId) + sizeof(TaskId) + sizeof(uint64_t) + sizeof(size_t) * 5;
  int32_t buffer_size = header_size + p_tids.size() + p_gids.size() + p_sids.size() + old.size();

  char *buffer = new char[buffer_size];

  size_t offset = 0;
  memcpy(buffer + offset, &maxCallBackId, sizeof(CallbackId));
  offset += sizeof(CallbackId);
  memcpy(buffer + offset, &maxTid, sizeof(TaskId));
  offset += sizeof(TaskId);
  memcpy(buffer + offset, &maxGid, sizeof(uint64_t));
  offset += sizeof(uint64_t);

  // compute tid offset;
  size_t tid_offset = header_size;
  memcpy(buffer + offset, &tid_offset, sizeof(size_t));
  offset += sizeof(size_t);

  // compute gid offset;
  size_t gid_offset = tid_offset + p_tids.size();
  memcpy(buffer + offset, &gid_offset, sizeof(size_t));
  offset += sizeof(size_t);

  // compute sid offset;
  size_t sid_offset = gid_offset + p_gids.size();
  memcpy(buffer + offset, &sid_offset, sizeof(size_t));
  offset += sizeof(size_t);

  // compute old_g2t offset;
  size_t og2t_offset = sid_offset + p_og2t.size();
  memcpy(buffer + offset, &og2t_offset, sizeof(size_t));
  offset += sizeof(size_t);

  // compute old offset;
  size_t old_offset = og2t_offset + p_sids.size();
  memcpy(buffer + offset, &old_offset, sizeof(size_t));

  memcpy(buffer + tid_offset, p_tids.buffer(), p_tids.size());
  memcpy(buffer + gid_offset, p_gids.buffer(), p_gids.size());
  memcpy(buffer + sid_offset, p_sids.buffer(), p_sids.size());
  memcpy(buffer + og2t_offset, p_og2t.buffer(), p_og2t.size());
  memcpy(buffer + old_offset, old.buffer(), old.size());

  delete[] p_tids.buffer();
  delete[] p_gids.buffer();
  delete[] p_sids.buffer();
  delete[] p_og2t.buffer();
  delete[] old.buffer();

  return Payload(buffer_size, buffer);
}

void SimplificationGraph::deserialize(BabelFlow::Payload payload) {
  char *buffer = payload.buffer();
  // deserialize header
  // header = |CallbackId|maxTid|maxGid|p_tids.offset|p_gids.offset|p_sids.offset|p_og2t.offset|old.offset|
  size_t tid_offset, gid_offset, sid_offset, og2t_offset, old_offset;
  size_t offset = 0;
  memcpy(&maxCallBackId, buffer + offset, sizeof(CallbackId));
  offset += sizeof(CallbackId);
  memcpy(&maxTid, buffer + offset, sizeof(TaskId));
  offset += sizeof(TaskId);
  memcpy(&maxGid, buffer + offset, sizeof(uint64_t));
  offset += sizeof(uint64_t);
  memcpy(&tid_offset, buffer + offset, sizeof(size_t));
  offset += sizeof(size_t);
  memcpy(&gid_offset, buffer + offset, sizeof(size_t));
  offset += sizeof(size_t);
  memcpy(&sid_offset, buffer + offset, sizeof(size_t));
  offset += sizeof(size_t);
  memcpy(&og2t_offset, buffer + offset, sizeof(size_t));
  offset += sizeof(size_t);
  memcpy(&old_offset, buffer + offset, sizeof(size_t));
  // deserialize maps
  new_tids = deserializeMap<TaskId, TaskId>(buffer + tid_offset);
  new_gids = deserializeMap<TaskId, uint64_t>(buffer + gid_offset);
  new_sids = deserializeMap<TaskId, ShardId>(buffer + sid_offset);
  old_g2t = deserializeMap<uint64_t, TaskId>(buffer + og2t_offset);
  // deserialize old - build Payload first
  Payload old(payload.size() - old_offset, buffer + old_offset);
  baseGraph->deserialize(old);

  delete[] payload.buffer();
}


