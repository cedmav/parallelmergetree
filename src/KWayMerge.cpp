/*
 * KWayMerge.cpp
 *
 *  Created on: Dec 15, 2014
 *      Author: bremer5
 */

#include <cassert>

#include "KWayMerge.h"
#include "MergeTree.h"
#include <sstream>

using namespace BabelFlow;

KWayMerge::KWayMerge(std::string config) {
  std::stringstream cmd(config);

  GlobalIndexType dim[3];

  cmd >> dim[0];
  cmd >> dim[1];
  cmd >> dim[2];

  uint32_t factor;

  cmd >> factor;

  init(dim, factor);
}

void KWayMerge::init(GlobalIndexType dim[3], uint32_t factor){

  mFactor = factor;
  uint32_t f;

  mLvlOffset.push_back(0);
  mLvlDim.push_back(std::vector<GlobalIndexType>(3));
  mLvlDim[0][0] = dim[0];
  mLvlDim[0][1] = dim[1];
  mLvlDim[0][2] = dim[2];

  assert ((factor == 2) || (factor == 4) || (factor == 8) ||
          (factor == 16) || (factor == 32));

  // Now we compute how many rounds
  mRounds = 1;

  while (dim[0] * dim[1] * dim[2] > 1) {
    mLvlOffset.push_back(mLvlOffset.back() + dim[0] * dim[1] * dim[2]);
    mRounds++;

    mFactors.push_back(std::vector<uint8_t>(3));
    mFactors.back()[0] = mFactors.back()[1] = mFactors.back()[2] = 1;

    f = 1;

    while ((f < factor) && (dim[0] * dim[1] * dim[2] > 1)) {
      if ((dim[0] >= dim[1]) && (dim[0] >= dim[2])) {
        dim[0] = (dim[0] + 1) / 2;
        mFactors.back()[0] *= 2;
      } else if (dim[1] >= dim[2]) {
        dim[1] = (dim[1] + 1) / 2;
        mFactors.back()[1] *= 2;
      } else {
        dim[2] = (dim[2] + 1) / 2;
        mFactors.back()[2] *= 2;
      }

      f *= 2;
    }

    mLvlDim.push_back(std::vector<GlobalIndexType>(3));
    mLvlDim.back()[0] = dim[0];
    mLvlDim.back()[1] = dim[1];
    mLvlDim.back()[2] = dim[2];
  }
  mLvlOffset.push_back(mLvlOffset.back() + 1);

  mFactors.push_back(std::vector<uint8_t>(3));
  mFactors.back()[0] = mFactors.back()[1] = mFactors.back()[2] = 1;

}


KWayMerge::KWayMerge(uint32_t dim[3], uint32_t factor) : mFactor(factor) {
  uint32_t f;

  mLvlOffset.push_back(0);
  mLvlDim.push_back(std::vector<GlobalIndexType>(3));
  mLvlDim[0][0] = dim[0];
  mLvlDim[0][1] = dim[1];
  mLvlDim[0][2] = dim[2];

  assert ((factor == 2) || (factor == 4) || (factor == 8) ||
          (factor == 16) || (factor == 32));

  // Now we compute how many rounds
  mRounds = 1;

  while (dim[0] * dim[1] * dim[2] > 1) {
    mLvlOffset.push_back(mLvlOffset.back() + dim[0] * dim[1] * dim[2]);
    mRounds++;

    mFactors.push_back(std::vector<uint8_t>(3));
    mFactors.back()[0] = mFactors.back()[1] = mFactors.back()[2] = 1;

    f = 1;

    while ((f < factor) && (dim[0] * dim[1] * dim[2] > 1)) {
      if ((dim[0] >= dim[1]) && (dim[0] >= dim[2])) {
        dim[0] = (dim[0] + 1) / 2;
        mFactors.back()[0] *= 2;
      } else if (dim[1] >= dim[2]) {
        dim[1] = (dim[1] + 1) / 2;
        mFactors.back()[1] *= 2;
      } else {
        dim[2] = (dim[2] + 1) / 2;
        mFactors.back()[2] *= 2;
      }

      f *= 2;
    }

    mLvlDim.push_back(std::vector<GlobalIndexType>(3));
    mLvlDim.back()[0] = dim[0];
    mLvlDim.back()[1] = dim[1];
    mLvlDim.back()[2] = dim[2];
  }
  mLvlOffset.push_back(mLvlOffset.back() + 1);

  mFactors.push_back(std::vector<uint8_t>(3));
  mFactors.back()[0] = mFactors.back()[1] = mFactors.back()[2] = 1;

  //for (uint8_t i=0;i<mRounds;i++)
  //fprintf(stderr,"Lvl %d: dim [%d,%d,%d], offset %d, factors [%d,%d,%d]\n",
  //        i,mLvlDim[i][0],mLvlDim[i][1],mLvlDim[i][2],mLvlOffset[i],
  //        mFactors[i][0],mFactors[i][1],mFactors[i][2]);

}

TaskId KWayMerge::size() const {
  assert (false); // Not meaningfull;
}

uint64_t KWayMerge::gId(BabelFlow::TaskId tId) const {

  uint8_t k;

  uint64_t gId = 0;
  BabelFlow::TaskId leaf_count = lvlOffset()[1];

  // For all leafs assigned to this controller
  for (BabelFlow::TaskId leaf = 0; leaf < leaf_count; leaf++) {
    if (leaf == tId)
      return gId;
    else
      gId++;

    // Now take its local copies for all rounds
    for (k = 1; k <= rounds(); k++) {
      if (roundId(leaf, k) == tId)
        return gId;
      else
        gId++;
    }
    // Walk down the tree until your child is no longer
    // assigned to the same controller
    uint8_t lvl = 0;
    BabelFlow::TaskId down = leaf;
    BabelFlow::TaskId next = reduce(down);
    while ((down != next) && (down == expand(next)[0])) {
      lvl++;
      down = next;

      if (lvl < rounds() - 1)
        next = reduce(next);

      if (down == tId)
        return gId;
      else
        gId++;

      // All lower nodes exist for all levels after this one
      for (k = lvl + 1; k < rounds(); k++) {
        if (roundId(down, k) == tId)
          return gId;
        else
          gId++;
      }
    }// end-while
  } // end-for all leafs

  return gId;

}

uint32_t KWayMerge::gatherTasks(BabelFlow::TaskId id) const {
  KWayTaskMap tmap(id, this);
  std::vector<BabelFlow::TaskId> ids = tmap.tasks(id);
  uint32_t c = 0;
  for (uint32_t i = 0; i < ids.size(); i++)
    if (gatherTask(ids[i])) c++;

  return c;
}

std::vector<Task> KWayMerge::localGraph(ShardId id,
                                        const TaskMap *task_map) const {
  std::vector<Task> tasks;

  auto ids = task_map->tasks(id);
  tasks.resize(ids.size());
  for (int i = 0; i < ids.size(); ++i) {
    tasks[i] = task(gId(ids[i]));
  }

  return tasks;
}

TaskId KWayMerge::toTId(uint64_t this_gId) const {

  uint8_t k;

  uint64_t gId = 0;
  BabelFlow::TaskId leaf_count = lvlOffset()[1];

  // For all leafs assigned to this controller
  for (BabelFlow::TaskId leaf = 0; leaf < leaf_count; leaf++) {
    if (gId == this_gId)
      return leaf;
    else
      gId++;

    // Now take its local copies for all rounds
    for (k = 1; k <= rounds(); k++) {
      if (gId == this_gId)
        return roundId(leaf, k);
      else
        gId++;
    }
    // Walk down the tree until your child is no longer
    // assigned to the same controller
    uint8_t lvl = 0;
    BabelFlow::TaskId down = leaf;
    BabelFlow::TaskId next = reduce(down);
    while ((down != next) && (down == expand(next)[0])) {
      lvl++;
      down = next;

      if (lvl < rounds() - 1)
        next = reduce(next);

      if (gId == this_gId)
        return down;
      else
        gId++;

      // All lower nodes exist for all levels after this one
      for (k = lvl + 1; k < rounds(); k++) {
        if (gId == this_gId)
          return roundId(down, k);
        else
          gId++;
      }
    }// end-while
  } // end-for all leafs

}

BabelFlow::Task KWayMerge::task(uint64_t this_gId) const {

  BabelFlow::Task task(toTId(this_gId));
  std::vector<BabelFlow::TaskId> incoming;
  std::vector<std::vector<BabelFlow::TaskId> > outgoing;
  if (gatherTask(task.id())) { // If this is part of the reduction

    if (task.id() < mLvlOffset[1]) { // If this is a leaf node
      task.callback(1); // Local compute

      incoming.resize(1); // One dummy input
      incoming[0] = TNULL;
      task.incoming(incoming);

      // Two output: (1) the boundary tree (2) the local information
      outgoing.resize(2);

      outgoing[0].resize(1);
      outgoing[0][0] = reduce(task.id()); // parent

      outgoing[1].resize(1);
      outgoing[1][0] = roundId(task.id(), 1); // Myself in round 1

      task.outputs(outgoing);

      //fprintf(stderr,"Leaf %d: outputs %d (%d,%d)\n",task.id(),
      //        outgoing[0][0],baseId(outgoing[1][0]),round(outgoing[1][0]));

    } // end-if leaf node
    else {
      //fprintf(stderr,"ID = %d\n",task.id());
      uint8_t lvl = level(task.id());

      // Join computation
      task.callback(2);

      // Directly compute all the incoming
      incoming = expand(task.id());
      task.incoming(incoming);

      // Unless it's a root use two outputs:
      // (1) Boundary tree (down) (2) augmented tree (up)
      int curr_idx = 0;
      outgoing.resize(1);
      if (task.id() != mLvlOffset.back() - 1) { // not a root -- two output
          outgoing.resize(2);
          outgoing[curr_idx].resize(1);
          outgoing[curr_idx][0] = reduce(task.id()); // parent
          curr_idx++;
      }

      // The up neighbors are the same as the incoming but at different lvl
      outgoing[curr_idx] = incoming;
      for (uint32_t k = 0; k < outgoing[curr_idx].size(); k++) {
        outgoing[curr_idx][k] = roundId(outgoing[curr_idx][k], lvl);
      }

      task.outputs(outgoing);

      //fprintf(stderr,"Merge %d: incoming %d %d outputs %d (%d,%d)\n",
      //        task.id(),incoming[0],incoming[1],outgoing[0][0],
      //        baseId(outgoing[1][0]),round(outgoing[1][0]));

    }
  }// end-if gatherTask()
  else { // This is a scatter task

    TaskId local = baseId(task.id()); // get the corresponding reduction id
    uint8_t lvl = level(local);
    uint8_t rnd = round(task.id());

    incoming.resize(1);
    incoming[0] = reduce(local);
    if (rnd > lvl + 1) // If this incoming is not the root of the scatter
      incoming[0] = roundId(incoming[0], rnd); // create the correct id

    task.incoming(incoming);

    if (local < mLvlOffset[1]) { // If this is a leaf node
      //printf("ROUNDS: %d , rnd : %d\n", rounds(), rnd);
      if (rnd < rounds()) {
        task.callback(3); // Local correction

        // We need the input from the previous round
        incoming.push_back(roundId(local, rnd - 1));
        task.incoming(incoming);


        // One output to the next round of corrections
        outgoing.resize(1);
        outgoing[0].resize(1);
        outgoing[0][0] = roundId(local, rnd + 1);

        task.outputs(outgoing);

        //printf("Leaf correction task : %d Incoming[0]: %d [1]: %d Outgoing: %d\n",
        //        task.id(), incoming[0], incoming[1], outgoing[0][0]);
      } else { // this is final task that does segmentation and writes output
        task.callback(4); // write the results to disk

        incoming[0] = roundId(local, rnd - 1);
        task.incoming(incoming);

        // An output with destination TNULL signifies an output to be extracted
        // from this task -- in this case, this is the final merged tree
        outgoing.resize(1);
        outgoing[0].resize(1, TNULL);

        task.outputs(outgoing);
      }
    } else { // Not a leaf node
      task.callback(0); // Relay task

      outgoing.resize(1);
      outgoing[0] = expand(local);
      for (uint32_t k = 0; k < outgoing[0].size(); k++) {
        outgoing[0][k] = roundId(outgoing[0][k], rnd);
      }

      task.outputs(outgoing);
    }
  }

  return task;
}

int KWayMerge::output_graph(ShardId count,
                            const TaskMap *task_map, FILE *output) {
  fprintf(output, "digraph G {\n");
  fprintf(output, "\trankdir=TB;ranksep=0.8;\n");

  for (uint8_t i = 0; i < mRounds; i++)
    fprintf(output, "f%d [label=\"level %d\"]", i, i);


  fprintf(output, "f0 ");
  for (uint8_t i = 1; i < mRounds; i++) {
    fprintf(output, " -> f%d", i);
  }
  fprintf(output, "\n\n");

  std::vector<Task> tasks;
  std::vector<Task>::iterator tIt;
  std::vector<TaskId>::iterator it;

  for (uint32_t i = 0; i < count; i++) {
    tasks = localGraph(i, task_map);


    for (tIt = tasks.begin(); tIt != tasks.end(); tIt++) {
      if (round(tIt->id()) == 0)
        fprintf(output, "%d [label=\"(%d, %d) ,%d)\",color=red]\n",
                tIt->id(), baseId(tIt->id()), round(tIt->id()), tIt->callback());
      else
        fprintf(output, "%d [label=\"(%d, %d) ,%d)\",color=black]\n",
                tIt->id(), baseId(tIt->id()), round(tIt->id()), tIt->callback());

      for (it = tIt->incoming().begin(); it != tIt->incoming().end(); it++) {
        if (*it != TNULL)
          fprintf(output, "%d -> %d\n", *it, tIt->id());
      }
    }

    for (tIt = tasks.begin(); tIt != tasks.end(); tIt++)
      fprintf(output, "{rank = same; f%d; %d}\n",
              level(baseId(tIt->id())), tIt->id());

  }

  fprintf(output, "}\n");
  return 1;
}


uint8_t KWayMerge::level(TaskId id) const {
  uint8_t l = 0;

  assert (gatherTask(id));

  // Figure out what level we are on
  while (id >= mLvlOffset[l + 1])
    l++;

  return l;
}

TaskId KWayMerge::roundId(TaskId id, uint8_t round) const {
  return (id | (round << sPostfixSize));
}


TaskId KWayMerge::reduce(TaskId source) const {
  uint32_t l = level(source);

  // Map the global id to an in-level grid row-major id
  if (l > 0)
    source -= mLvlOffset[l];

  assert (l < mLvlOffset.size() - 1);

  // Map this to an in-level row major id on the next level
  source = gridReduce(source, l);

  // Finally map it back to a global id
  source += mLvlOffset[l + 1];

  return source;
}

std::vector<TaskId> KWayMerge::expand(TaskId source) const {
  uint32_t l = level(source);

  assert (l > 0);

  // Map the global id to an in-level grid row-major id
  if (l > 0)
    source -= mLvlOffset[l];


  // Get the row-major indices for the lower level
  std::vector<TaskId> up = gridExpand(source, l);

  // Convert them to global indices
  for (uint32_t i = 0; i < up.size(); i++)
    up[i] += mLvlOffset[l - 1];

  return up;
}


TaskId KWayMerge::gridReduce(TaskId source, uint8_t lvl) const {
  TaskId p[3];

  assert (lvl < mLvlDim.size() - 1);

  // Compute the current index
  p[0] = source % mLvlDim[lvl][0];
  p[1] = ((source - p[0]) / mLvlDim[lvl][0]) % mLvlDim[lvl][1];
  p[2] = source / (mLvlDim[lvl][0] * mLvlDim[lvl][1]);

  // Adapt the indices
  p[0] = p[0] / mFactors[lvl][0];
  p[1] = p[1] / mFactors[lvl][1];
  p[2] = p[2] / mFactors[lvl][2];


  // Compute the new index
  source = (p[2] * mLvlDim[lvl + 1][1] + p[1]) * mLvlDim[lvl + 1][0] + p[0];

  return source;
}

std::vector<TaskId> KWayMerge::gridExpand(TaskId source, uint8_t lvl) const {
  TaskId p[3];
  std::vector<TaskId> up;

  assert (lvl > 0);

  // Compute the current index
  p[0] = source % mLvlDim[lvl][0];
  p[1] = ((source - p[0]) / mLvlDim[lvl][0]) % mLvlDim[lvl][1];
  p[2] = source / (mLvlDim[lvl][0] * mLvlDim[lvl][1]);

  // Adapt the indices
  p[0] = p[0] * mFactors[lvl - 1][0];
  p[1] = p[1] * mFactors[lvl - 1][1];
  p[2] = p[2] * mFactors[lvl - 1][2];

  // Compute the new indices
  for (TaskId i = 0; i < mFactors[lvl - 1][2]; i++) {
    for (TaskId j = 0; j < mFactors[lvl - 1][1]; j++) {
      for (TaskId k = 0; k < mFactors[lvl - 1][0]; k++) {

        if ((p[0] + k < mLvlDim[lvl - 1][0]) && (p[1] + j < mLvlDim[lvl - 1][1]) && (p[2] + i < mLvlDim[lvl - 1][2]))
          up.push_back(((p[2] + i) * mLvlDim[lvl - 1][1] + (p[1] + j)) * mLvlDim[lvl - 1][0] + (p[0] + k));
      }
    }
  }

  return up;
}

BabelFlow::Payload KWayMerge::serialize() const
{
  GlobalIndexType* buffer = new GlobalIndexType[8];

  buffer[0] = mLvlDim[0][0];
  buffer[1] = mLvlDim[0][1];
  buffer[2] = mLvlDim[0][2];

  buffer[3] = MergeTree::sDimension[0];
  buffer[4] = MergeTree::sDimension[1];
  buffer[5] = MergeTree::sDimension[2];
  buffer[6] = mFactor;

  // TODO threshold

  //printf("serializing %d %d %d , f %d\n", buffer[0], buffer[1], buffer[2], buffer[3]);

  return Payload(8*sizeof(GlobalIndexType),(char*)buffer);
}

void KWayMerge::deserialize(BabelFlow::Payload buffer)
{
  assert (buffer.size() == 8*sizeof(GlobalIndexType));
  GlobalIndexType *tmp = (GlobalIndexType *)(buffer.buffer());

  // mLvlDim[0][0] = tmp[0];
  // mLvlDim[0][1] = tmp[1];
  // mLvlDim[0][2] = tmp[2];
  mFactor = tmp[6];

  //printf("DEserializing %d %d %d , f %d\n", tmp[0], tmp[1], tmp[2], tmp[3]);

  GlobalIndexType* decomp = tmp;
  GlobalIndexType* dim = tmp+3;

  MergeTree::setDimension(dim);
  // memcpy(dim, buffer.buffer(), sizeof(uint32_t)*3);

  init(decomp, mFactor);

  delete[] buffer.buffer();
}
