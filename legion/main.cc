/*
 *  main.cc
 *
 *  Created on: Mar 20, 2016
 *      Author: spetruzza
 */

#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <cfloat>
#include <fstream>
#include <sys/time.h>
#include "legion.h"

#include "KWayTaskMap.h"
#include "KWayMerge.h"
#include "MergeTree.h"
#include "AugmentedMergeTree.h"
#include "TypeDefinitions.h"
#include "SortedJoinAlgorithm.h"
#include "LocalCorrectionAlgorithm.h"
#include "BabelFlow/legion/Controller.h"
#include "BabelFlow/legion/Utils.h"
#include "SortedUnionFindAlgorithm.h"
//#include "HierarchicalKWayMerge.h"

#include "../idxio/read_block.h"
#ifdef VISUS_IDXIO
#include <Visus/IdxDataset.h>
#include <Visus/Kernel.h>
#endif

#ifdef USE_GASNET
#include <gasnet.h>
#endif

using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;

using namespace BabelFlow;

#define DETAILED_TIMING 1
#define DO_SEGMENTATION 1

#define WRITE_RESULTS 0 && DO_SEGMENTATION

//! The number of bits used for prefixing scatter tasks
static const uint8_t sPrefixSize = 4;

//! The number of non-prefix bits
static const uint8_t sPostfixSize = sizeof(TaskId)*8 - sPrefixSize;

//! Bit mask for scatter tasks
static const TaskId sPrefixMask = ((1 << sPrefixSize) - 1) << sPostfixSize;

int local_compute(std::vector<Payload>& inputs,
                  std::vector<Payload>& output, TaskId task){
#if DETAILED_TIMING || PROFILING_TIMING
  std::cout << std::fixed << "compute start " <<Realm::Clock::current_time_in_microseconds()/1000000.0f <<std::endl;
#endif

  sorted_union_find_algorithm(inputs, output, task);
 
//  MergeTree t;
//
//  fprintf(stderr,"LOCAL COMPUTE performed by task %d\n", task);
//  t.decode(output[0]);
//
//  t.writeToFile(task);

  // Deleting input data
  for (int i=0; i<inputs.size(); i++){
    delete[] (char*)inputs[i].buffer();
  }
  inputs.clear();
  
  return 1;
}


int join(std::vector<Payload>& inputs,
         std::vector<Payload>& output, TaskId task){
 
  //fprintf(stderr, "Task : %d : Started with join algorithm\n", task);
  sorted_join_algorithm(inputs, output, task);
  //fprintf(stderr, "Task : %d : Done with join algorithm\n", task);
  
 /* MergeTree join_tree;
  
  join_tree.decode(output[0]);
  join_tree.writeToFile(task+1000);
  */
  // Deleting input data
  for (int i=0; i<inputs.size(); i++){
    delete[] (char*)inputs[i].buffer();
  }
  inputs.clear();
  
  return 0;
}

int local_correction(std::vector<Payload>& inputs,
                     std::vector<Payload>& output, TaskId task){
  if(inputs[0].size() > inputs[1].size()){
    fprintf(stderr,"ERROR: Found inverted input\n");
    assert(false);
  }

  //if ((task & ~sPrefixMask) == 237)
  local_correction_algorithm(inputs, output, task);
  
  // Deleting input data
  for (int i=0; i<inputs.size(); i++){
    delete[] (char*)inputs[i].buffer();
  }
  inputs.clear();
  
  //fprintf(stderr,"CORRECTION performed by task %d\n", task & ~sPrefixMask);
  return 1;
}

int write_results(std::vector<Payload>& inputs,
                  std::vector<Payload>& output, TaskId task){
  
  AugmentedMergeTree t;
  t.decode(inputs[0]);
  
  t.id(task & ~sPrefixMask);
  //t.writeToFile(task & ~sPrefixMask);
 // printf("after write to file %d.dot\n", task & ~sPrefixMask);
#if DO_SEGMENTATION
  t.computeSegmentation();
#endif

#if DETAILED_TIMING || PROFILING_TIMING
  std::cout << std::fixed << "compute end " <<Realm::Clock::current_time_in_microseconds()/1000000.0f <<std::endl;
#endif

#if WRITE_RESULTS
  t.writeToFileBinary(task & ~sPrefixMask);
  //fprintf(stderr,"done segmentation\n");
#endif

  // Deleting input data
  for (int i=0; i<inputs.size(); i++){
    delete[] (char*)inputs[i].buffer();
  }
  inputs.clear();
  
  assert(output.size() == 0);
  //fprintf(stderr,"WRITING RESULTS performed by %d\n", task & ~sPrefixMask);
  return 1;
}

template <typename T>
struct DomainBlock{
  GlobalIndexType low[3];
  GlobalIndexType high[3];
  T* data;
};

template<typename T>
T* extractBlock(T* data, uint32_t* dim, uint32_t* start, uint32_t* end){
  uint32_t dim_block[3];
  
  for(int i=0; i < 3; i++)
      dim_block[i] = end[i]-start[i]+1;
  
  T* block = (T*)malloc(sizeof(T)*dim_block[0]*dim_block[1]*dim_block[2]);
  
  uint32_t bidx = 0;
  
  for (uint32_t z = start[2]; z < end[2]+1; z++) {
    for (uint32_t y = start[1]; y < end[1]+1; y++) {
      for (uint32_t x = start[0]; x < end[0]+1; x++) {
        
            //printf("ext %d %d %d\n", x, y, z);
            uint32_t idx = x + y * dim[0] + z * dim[0]*dim[1];
            
            block[bidx++] = data[idx];
            
          }
      }
  }

  //printf("dims %d %d %d\n", dim_block[0], dim_block[1], dim_block[2]);
  //printf("written %d\n", bidx);
  
  return block;
  
}

template<typename T>
std::vector<DomainBlock<T> > blockify(T* data, uint32_t* num_blocks, GlobalIndexType* dim, int share_face){
  
  std::vector<DomainBlock<T> > blocks;
  
  /*uint32_t num_blocks[3] = {(dim[0]+(dim_block[0]-1))/dim_block[0],
    (dim[1]+(dim_block[1]-1))/dim_block[1],
    (dim[2]+(dim_block[2]-1))/dim_block[2]};
  */
  uint32_t dim_block[3] = {dim[0]/num_blocks[0], dim[1]/num_blocks[1],dim[2]/num_blocks[2]};
  
  int num_blocks_count = num_blocks[0]*num_blocks[1]*num_blocks[2];
  
//  printf("Blockify size %dx%dx%d into %dx%dx%d blocks...\n", dim[0], dim[1], dim[2], dim_block[0], dim_block[1], dim_block[2]);
//  printf("Num %dx%dx%d=%d blocks...\n", num_blocks[0], num_blocks[1], num_blocks[2], num_blocks[0]*num_blocks[1]*num_blocks[2]);
  
  Rect<1> color_bounds(Point<1>(0),Point<1>(num_blocks_count-1));
  
  IndexPartition ip;
  DomainColoring coloring;
  
  GlobalIndexType b_start[3];
  GlobalIndexType b_end[3];
  GlobalIndexType b_dim[3];
  
  for (uint32_t z = 0, idx = 0; z < num_blocks[2]; ++z) {
    b_start[2] = z * dim_block[2];
    b_dim[2] = ((b_start[2] + dim_block[2]) <= dim[2]) ?
    dim_block[2] : (dim[2] - b_start[2]);
    b_end[2] = b_start[2] + b_dim[2] -1;
    
    if(b_end[2] + share_face < dim[2]) b_end[2] = b_end[2] + share_face;
    
    for (uint32_t y = 0; y < num_blocks[1]; ++y) {
      b_start[1] = y * dim_block[1];
      b_dim[1] = ((b_start[1] + dim_block[1]) <= dim[1]) ?
      dim_block[1] : (dim[1] - b_start[1]);
      b_end[1] = b_start[1] + b_dim[1] -1;
      
      if(b_end[1] + share_face < dim[1]) b_end[1] = b_end[1] + share_face;
      
      for (uint32_t x = 0; x < num_blocks[0]; ++x, ++idx) {
        b_start[0] = x * dim_block[0];
        b_dim[0] = ((b_start[0] + dim_block[0]) <= dim[0]) ?
        dim_block[0] : (dim[0] - b_start[0]);
        b_end[0] = b_start[0] + b_dim[0] - 1;
        
        if(b_end[0] + share_face < dim[0]) b_end[0] = b_end[0] + share_face;
        
        DomainBlock<T> block;
        memcpy(block.low, b_start, sizeof(GlobalIndexType)*3);
        memcpy(block.high, b_end, sizeof(GlobalIndexType)*3);

       // printf("block %d start (%d %d %d) end (%d %d %d)\n", idx, b_start[0],b_start[1], b_start[2], b_end[0],b_end[1], b_end[2]);
        
        block.data = extractBlock<T>(data, dim, b_start, b_end);
        
        blocks.push_back(block);
        
      }
    }
  }
  
  return  blocks;
}


std::map<TaskId,Payload> input_initialization(int argc, char* argv[]){
  GlobalIndexType data_size[3];        // {x_size, y_size, z_size}
  uint32_t block_decomp[3];     // block decomposition
  int nblocks;                  // number of blocks per input task
  int share_face = 1;           // share a face among the blocks
  uint32_t valence = 2;
  FunctionType threshold = (FunctionType)(-1)*FLT_MAX;
  char* dataset = "";
  fprintf(stderr, "input initialization started\n");
  for (int i = 1; i < argc; i++){
    if (!strcmp(argv[i],"-d")){
      data_size[0] = atoi(argv[++i]);
      data_size[1] = atoi(argv[++i]);
      data_size[2] = atoi(argv[++i]);
    }
    if (!strcmp(argv[i],"-p")){
      block_decomp[0] = atoi(argv[++i]);
      block_decomp[1] = atoi(argv[++i]);
      block_decomp[2] = atoi(argv[++i]);
    }
    if (!strcmp(argv[i],"-m"))
      valence = atoi(argv[++i]);
    if (!strcmp(argv[i],"-t"))
      threshold = atof(argv[++i]);
    if (!strcmp(argv[i],"-f"))
      dataset = argv[++i];
  }
  
  KWayMerge graph(block_decomp, valence);
  KWayTaskMap task_map(1, &graph);
  std::map<TaskId,Payload> initial_input;
  MergeTree::setDimension(data_size);

  std::ifstream in;
  in.open(dataset);
  
  uint32_t tot_size = data_size[0]*data_size[1]*data_size[2]*sizeof(float);
  float *data = (float*)malloc(tot_size);
  
  in.read((char*)data, tot_size);
  
  std::vector<DomainBlock<float> > blocks = blockify<float>(data, block_decomp, data_size, share_face);
  nblocks = blocks.size();
  /*
  FILE* output = fopen("graph.dot","w");
  graph.output_graph(1, &task_map, output);
  fclose(output);
  */
  std::vector<BabelFlow::Task> alltasks = graph.localGraph(0, &task_map);
  std::map<TaskId,BabelFlow::Task> taskmap;
  
  std::vector<BabelFlow::Task> leafTasks;
  
  for(uint32_t i=0; i < alltasks.size(); i++){
    taskmap[alltasks[i].id()] = alltasks[i];
    
    if(alltasks[i].incoming().size() > 0 && alltasks[i].incoming()[0] == TNULL){
      leafTasks.push_back(alltasks[i]);
      //    printf("leaf task %d\n", alltasks[i].id());
    }
  }
  
  //std::map<TaskId,Payload> initial_input;
  
  int in_length = leafTasks.size();
  
  // Set input for leaf tasks
  for(int i=0; i < in_length; i++){
    
    BabelFlow::Task& task = leafTasks[i];
    //  printf("input task %d callback %d\n", task.id(), task.callback());
    initial_input[task.id()] =  make_local_block((float*)(blocks[i].data), blocks[i].low, blocks[i].high, threshold);//inputs[i];
  }

  in.close();
  free(data);

  fprintf(stderr,"input initialization done\n");
  
  return initial_input;
}

#if USE_SPMD_CONTROLLER
std::vector<InputDomainSelection> blockify_nodata(uint32_t* num_blocks, GlobalIndexType* dim, int share_face){
  
  std::vector<InputDomainSelection> blocks;
  
  /*uint32_t num_blocks[3] = {(dim[0]+(dim_block[0]-1))/dim_block[0],
    (dim[1]+(dim_block[1]-1))/dim_block[1],
    (dim[2]+(dim_block[2]-1))/dim_block[2]};
  */
  uint32_t dim_block[3] = {dim[0]/num_blocks[0], dim[1]/num_blocks[1],dim[2]/num_blocks[2]};
  
  int num_blocks_count = num_blocks[0]*num_blocks[1]*num_blocks[2];
  
  //printf("Blockify size %dx%dx%d into %dx%dx%d blocks...\n", dim[0], dim[1], dim[2], dim_block[0], dim_block[1], dim_block[2]);
  //printf("Num %dx%dx%d=%d blocks...\n", num_blocks[0], num_blocks[1], num_blocks[2], num_blocks[0]*num_blocks[1]*num_blocks[2]);
    
  GlobalIndexType b_start[3];
  GlobalIndexType b_end[3];
  GlobalIndexType b_dim[3];
  
  for (uint32_t z = 0, idx = 0; z < num_blocks[2]; ++z) {
    b_start[2] = z * dim_block[2];
    b_dim[2] = ((b_start[2] + dim_block[2]) <= dim[2]) ?
    dim_block[2] : (dim[2] - b_start[2]);
    b_end[2] = b_start[2] + b_dim[2] -1;

    if(b_end[2] + share_face < dim[2]) b_end[2] = b_end[2] + share_face;
    
    for (uint32_t y = 0; y < num_blocks[1]; ++y) {
      b_start[1] = y * dim_block[1];
      b_dim[1] = ((b_start[1] + dim_block[1]) <= dim[1]) ?
      dim_block[1] : (dim[1] - b_start[1]);
      b_end[1] = b_start[1] + b_dim[1] -1;
      
      if(b_end[1] + share_face < dim[1]) b_end[1] = b_end[1] + share_face;
      
      for (uint32_t x = 0; x < num_blocks[0]; ++x, ++idx) {
        b_start[0] = x * dim_block[0];
        b_dim[0] = ((b_start[0] + dim_block[0]) <= dim[0]) ?
        dim_block[0] : (dim[0] - b_start[0]);
        b_end[0] = b_start[0] + b_dim[0] - 1;
        
        if(b_end[0] + share_face < dim[0]) b_end[0] = b_end[0] + share_face;
        
        InputDomainSelection block;
        memcpy(block.low, b_start, sizeof(GlobalIndexType)*3);
        memcpy(block.high, b_end, sizeof(GlobalIndexType)*3);

        //printf("block %d start (%d %d %d) end (%d %d %d)\n", idx, b_start[0],b_start[1], b_start[2], b_end[0],b_end[1], b_end[2]);
        
        blocks.push_back(block);
        
      }
    }
  }
  
  return  blocks;
}

std::map<TaskId,Payload> input_initialization_nodata(int argc, char* argv[]){
  GlobalIndexType data_size[3];        // {x_size, y_size, z_size}
  uint32_t in_block_decomp[3];     // block decomposition
  int nblocks;                  // number of blocks per input task
  int share_face = 1;           // share a face among the blocks
  uint32_t valence = 2;
  FunctionType threshold = (FunctionType)(-1)*FLT_MAX;
  char* dataset = "";
  //fprintf(stderr, "input initialization started\n");
  for (int i = 1; i < argc; i++){
    if (!strcmp(argv[i],"-d")){
      data_size[0] = atoi(argv[++i]);
      data_size[1] = atoi(argv[++i]);
      data_size[2] = atoi(argv[++i]);
    }
    if (!strcmp(argv[i],"-p")){
      in_block_decomp[0] = atoi(argv[++i]);
      in_block_decomp[1] = atoi(argv[++i]);
      in_block_decomp[2] = atoi(argv[++i]);
    }
    if (!strcmp(argv[i],"-m"))
      valence = atoi(argv[++i]);
    if (!strcmp(argv[i],"-t"))
      threshold = atof(argv[++i]);
    if (!strcmp(argv[i],"-f"))
      dataset = argv[++i];
  }

  uint32_t block_decomp[3];
  memcpy(block_decomp, in_block_decomp, 3*sizeof(uint32_t));

  KWayMerge graph(in_block_decomp, valence);

  KWayTaskMap task_map(1, &graph);
  std::map<TaskId,Payload> initial_input;
  MergeTree::setDimension(data_size);

  std::ifstream in;
  in.open(dataset);

  GlobalIndexType tot_size = data_size[0]*data_size[1]*data_size[2]*sizeof(float);
  float *data = (float*)malloc(tot_size);

  in.read((char*)data, tot_size);

  std::vector<InputDomainSelection> blocks = blockify_nodata(block_decomp, data_size, share_face);
  nblocks = blocks.size();
  /*
  FILE* output = fopen("graph.dot","w");
  graph.output_graph(1, &task_map, output);
  fclose(output);
  */
  std::vector<BabelFlow::Task> alltasks = graph.localGraph(0, &task_map);
  std::map<TaskId,BabelFlow::Task> taskmap;

  std::vector<BabelFlow::Task> leafTasks;

  for(uint32_t i=0; i < alltasks.size(); i++){
    taskmap[alltasks[i].id()] = alltasks[i];

    if(alltasks[i].incoming().size() > 0 && alltasks[i].incoming()[0] == TNULL){
      leafTasks.push_back(alltasks[i]);
      //    printf("leaf task %d\n", alltasks[i].id());
    }
  }

  //std::map<TaskId,Payload> initial_input;

  int in_length = leafTasks.size();

  // Set input for leaf tasks
  for(int i=0; i < in_length; i++){

    BabelFlow::Task& task = leafTasks[i];
    //    printf("input task %d callback %d\n", task.id(), task.callback());

    size_t size = sizeof(InputDomainSelection);
    char* input = (char*)malloc(size);

    memcpy(input, &blocks[i], size);
    Payload pay(size, input);

    initial_input[task.id()] = pay;
  }

  in.close();
  free(data);

  // fprintf(stderr,"input initialization done\n");

  return initial_input;
}

#else

std::vector<DomainSelection> blockify_nodata(uint32_t* num_blocks, GlobalIndexType* dim, int share_face){

  std::vector<DomainSelection> blocks;

  /*uint32_t num_blocks[3] = {(dim[0]+(dim_block[0]-1))/dim_block[0],
    (dim[1]+(dim_block[1]-1))/dim_block[1],
    (dim[2]+(dim_block[2]-1))/dim_block[2]};
  */
  uint32_t dim_block[3] = {dim[0]/num_blocks[0], dim[1]/num_blocks[1],dim[2]/num_blocks[2]};

  int num_blocks_count = num_blocks[0]*num_blocks[1]*num_blocks[2];

  printf("Blockify size %dx%dx%d into %dx%dx%d blocks...\n", dim[0], dim[1], dim[2], dim_block[0], dim_block[1], dim_block[2]);
  printf("Num %dx%dx%d=%d blocks...\n", num_blocks[0], num_blocks[1], num_blocks[2], num_blocks[0]*num_blocks[1]*num_blocks[2]);

  GlobalIndexType b_start[3];
  GlobalIndexType b_end[3];
  GlobalIndexType b_dim[3];

  for (uint32_t z = 0, idx = 0; z < num_blocks[2]; ++z) {
    b_start[2] = z * dim_block[2];
    b_dim[2] = ((b_start[2] + dim_block[2]) <= dim[2]) ?
               dim_block[2] : (dim[2] - b_start[2]);
    b_end[2] = b_start[2] + b_dim[2] -1;

    if(b_end[2] + share_face < dim[2]) b_end[2] = b_end[2] + share_face;

    for (uint32_t y = 0; y < num_blocks[1]; ++y) {
      b_start[1] = y * dim_block[1];
      b_dim[1] = ((b_start[1] + dim_block[1]) <= dim[1]) ?
                 dim_block[1] : (dim[1] - b_start[1]);
      b_end[1] = b_start[1] + b_dim[1] -1;

      if(b_end[1] + share_face < dim[1]) b_end[1] = b_end[1] + share_face;

      for (uint32_t x = 0; x < num_blocks[0]; ++x, ++idx) {
        b_start[0] = x * dim_block[0];
        b_dim[0] = ((b_start[0] + dim_block[0]) <= dim[0]) ?
                   dim_block[0] : (dim[0] - b_start[0]);
        b_end[0] = b_start[0] + b_dim[0] - 1;

        if(b_end[0] + share_face < dim[0]) b_end[0] = b_end[0] + share_face;

        DomainSelection block;
        memcpy(block.low, b_start, sizeof(GlobalIndexType)*3);
        memcpy(block.high, b_end, sizeof(GlobalIndexType)*3);

        // printf("block %d start (%d %d %d) end (%d %d %d)\n", idx, b_start[0],b_start[1], b_start[2], b_end[0],b_end[1], b_end[2]);

        blocks.push_back(block);

      }
    }
  }

  return  blocks;
}

std::map<TaskId,Payload> input_initialization_nodata(int argc, char* argv[]){
  GlobalIndexType data_size[3];        // {x_size, y_size, z_size}
  uint32_t block_decomp[3];     // block decomposition
  int nblocks;                  // number of blocks per input task
  int share_face = 1;           // share a face among the blocks
  uint32_t valence = 2;
  FunctionType threshold = (FunctionType)(-1)*FLT_MAX;
  char* dataset = "";
  //fprintf(stderr, "input initialization started\n");
  for (int i = 1; i < argc; i++){
    if (!strcmp(argv[i],"-d")){
      data_size[0] = atoi(argv[++i]);
      data_size[1] = atoi(argv[++i]);
      data_size[2] = atoi(argv[++i]);
    }
    if (!strcmp(argv[i],"-p")){
      block_decomp[0] = atoi(argv[++i]);
      block_decomp[1] = atoi(argv[++i]);
      block_decomp[2] = atoi(argv[++i]);
    }
    if (!strcmp(argv[i],"-m"))
      valence = atoi(argv[++i]);
    if (!strcmp(argv[i],"-t"))
      threshold = atof(argv[++i]);
    if (!strcmp(argv[i],"-f"))
      dataset = argv[++i];
  }

  uint32_t block_decomp_2[3]={block_decomp[0],block_decomp[1],block_decomp[2]};

  KWayMerge graph(block_decomp, valence);
  KWayTaskMap task_map(1, &graph);
  std::map<TaskId,Payload> initial_input;
  MergeTree::setDimension(data_size);

  // TODO Move this first before graph adn remove copy
  std::vector<DomainSelection> blocks = blockify_nodata(block_decomp_2, data_size, share_face);

//  std::ifstream in;
//  in.open(dataset);
//
//  GlobalIndexType tot_size = data_size[0]*data_size[1]*data_size[2]*sizeof(float);
//  float *data = (float*)malloc(tot_size);
//
//  in.read((char*)data, tot_size);

  nblocks = blocks.size();
  /*
  FILE* output = fopen("graph.dot","w");
  graph.output_graph(1, &task_map, output);
  fclose(output);
  */
  std::vector<BabelFlow::Task> alltasks = graph.localGraph(0, &task_map);
  std::map<TaskId,BabelFlow::Task> taskmap;

  std::vector<BabelFlow::Task> leafTasks;

  for(uint32_t i=0; i < alltasks.size(); i++){
    taskmap[alltasks[i].id()] = alltasks[i];

    if(alltasks[i].incoming().size() > 0 && alltasks[i].incoming()[0] == TNULL){
      leafTasks.push_back(alltasks[i]);
      //    printf("leaf task %d\n", alltasks[i].id());
    }
  }

  //std::map<TaskId,Payload> initial_input;

  int in_length = leafTasks.size();

  // Set input for leaf tasks
  for(int i=0; i < in_length; i++){

    BabelFlow::Task& task = leafTasks[i];
    //    printf("input task %d callback %d\n", task.id(), task.callback());

    size_t size = sizeof(DomainSelection);
    char* input = (char*)malloc(size);

    memcpy(input, &blocks[i], size);
    Payload pay(size, input);

    initial_input[task.id()] = pay;
  }

//  in.close();
//  free(data);

  // fprintf(stderr,"input initialization done\n");

  return initial_input;
}
#endif



void output_full_graph(FILE* output, std::vector<BabelFlow::Task>& tasks){
    fprintf(output,"digraph G {\n");
    fprintf(output,"\trankdir=TB;ranksep=0.8;\n");
  
    std::vector<BabelFlow::Task>::iterator tIt;
    std::vector<TaskId>::iterator it;

    char** colors = new char*[5];
    colors[0] = "1";
    colors[1] = "2";
    colors[2] = "3";
    colors[3] = "4";
    colors[4] = "5";

    for (tIt=tasks.begin();tIt!=tasks.end();tIt++) {

      fprintf(output,"%d [label=\"%d,%d\",colorscheme=gnbu5,color=%s]\n",
            tIt->id(), tIt->id(), tIt->callback(), colors[tIt->callback()]);
      
        for (it=tIt->incoming().begin();it!=tIt->incoming().end();it++) {
          if (*it != TNULL)
            fprintf(output,"%d -> %d\n",*it,tIt->id());
        }
      }

    fprintf(output,"}\n");

  }

int this_argc;
char **this_argv;

//#define READ_DUMP_TEST

#if USE_SPMD_CONTROLLER
bool load_task(const Legion::Task *task,
                           const std::vector<PhysicalRegion> &regions,
                           Context ctx, Runtime *runtime){
  //printf("entering arglen %d\n", task->local_arglen);
  Domain dom_incall = runtime->get_index_space_domain(ctx, task->regions[0].region.get_index_space());
  Rect<1> elem_rect_in = dom_incall.get_rect<1>();

  //printf("from task %lld volume %d!\n", task->index_point.point_data[0], elem_rect_in.volume());
  //assert(task->local_arglen == sizeof(InputDomainSelection));
  assert(task->arglen == sizeof(InputDomainSelection));

  InputDomainSelection box = *(InputDomainSelection*)task->args;//task->local_args;//args;
  /*if(elem_rect_in.volume() == 0){
    fprintf(stdout,"invalid rect %llu %llu\n", elem_rect_in.lo,elem_rect_in.hi);
    fprintf(stdout,"invalid data %d %d %d - %d %d %d \n", box.low[0],box.low[1],box.low[2],box.high[0],box.high[1],box.high[2]);
    assert(false);
    }*/

  FunctionType threshold = (FunctionType)(-1)*FLT_MAX;
  char* dataset = NULL;
  //fprintf(stderr, "input initialization started\n");

  for (int i = 1; i < this_argc; i++){
    if (!strcmp(this_argv[i],"-t"))
      threshold = atof(this_argv[++i]);
    if (!strcmp(this_argv[i],"-f"))
      dataset = this_argv[++i];
  }

  fprintf(stdout,"Load data %d %d %d - %d %d %d thr %f\n", box.low[0],box.low[1],box.low[2],box.high[0],box.high[1],box.high[2], threshold);

  char* data_block;

  data_block = read_block(dataset,box.low,box.high);

  BabelFlow::Payload pay = make_local_block((FunctionType*)(data_block), box.low, box.high, threshold);

#ifdef READ_DUMP_TEST
   char filename[128];
   sprintf(filename,"dump_%d_%d_%d-%d_%d_%d.raw", box.low[0],box.low[1],box.low[2], box.high[0],box.high[1],box.high[2]);
   std::ofstream outfile (filename,std::ofstream::binary);

   int32_t datasize = (box.high[0]-box.low[0]+1)*(box.high[1]-box.low[1]+1)*(box.high[2]-box.low[2]+1)*sizeof(FunctionType);
   outfile.write(pay.buffer(),datasize);

   outfile.close();
#endif

  // char filename[128];
  // sprintf(filename, "outblock_%d.raw", task->index_point.point_data[0]);
  // std::ofstream outfile (filename,std::ofstream::binary);
  // outfile.write(pay.buffer(), pay.size());
  // outfile.close();

  //printf("volume in %d size %d\n", task->index_point.point_data[0], pay.size());
  assert(elem_rect_in.volume() >= pay.size()/BYTES_PER_POINT);

  Controller::bufferToRegion(pay.buffer(), pay.size()/BYTES_PER_POINT, elem_rect_in, regions[0]);//&ctx, runtime);
  //runtime->unmap_region(ctx,regions[0]);

  // char offname[128];
  //   sprintf(offname,"read_%d.raw", RegionsIndexType(elem_rect_in.lo));
  //   std::ofstream outresfile(offname,std::ofstream::binary);

  //    outresfile.write(reinterpret_cast<char*>(pay.buffer()),pay.size());

  //    outresfile.close();

  delete [] pay.buffer();

  return true;
}

#else

// Task for data loading
LogicalRegion load_task(const Legion::Task *task,
                                    const std::vector<PhysicalRegion> &regions,
                                    Context ctx, HighLevelRuntime *runtime){
  //printf("entering arglen %d\n", task->local_arglen);
  Domain dom_incall = runtime->get_index_space_domain(ctx, task->regions[0].region.get_index_space());
  Rect<1> elem_rect_in = dom_incall.get_rect<1>();

  //printf("from task %lld volume %d!\n", task->index_point.point_data[0], elem_rect_in.volume());
  assert(task->local_arglen == sizeof(DomainSelection));

  DomainSelection box = *(DomainSelection*)task->local_args;//args;
  /*if(elem_rect_in.volume() == 0){
    fprintf(stdout,"invalid rect %llu %llu\n", elem_rect_in.lo,elem_rect_in.hi);
    fprintf(stdout,"invalid data %d %d %d - %d %d %d \n", box.low[0],box.low[1],box.low[2],box.high[0],box.high[1],box.high[2]);
    assert(false);
    }*/

  FunctionType threshold = (FunctionType)(-1)*FLT_MAX;
  char* dataset = NULL;
  //fprintf(stderr, "input initialization started\n");

  for (int i = 1; i < this_argc; i++){
    if (!strcmp(this_argv[i],"-t"))
      threshold = atof(this_argv[++i]);
    if (!strcmp(this_argv[i],"-f"))
      dataset = this_argv[++i];
  }

  fprintf(stdout,"Load data %d %d %d - %d %d %d thr %f\n", box.low[0],box.low[1],box.low[2],box.high[0],box.high[1],box.high[2], threshold);

  char* data_block = read_block(dataset,box.low,box.high);

#ifdef READ_DUMP_TEST
  char filename[128];
   sprintf(filename,"dump_%d_%d_%d-%d_%d_%d.raw", box.low[0],box.low[1],box.low[2], box.high[0],box.high[1],box.high[2]);
   std::ofstream outfile (filename,std::ofstream::binary);

   int32_t datasize = (box.high[0]-box.low[0]+1)*(box.high[1]-box.low[1]+1)*(box.high[2]-box.low[2]+1)*sizeof(FunctionType);
   outfile.write(pay.buffer(),datasize);

   outfile.close();
#endif

  Payload pay = make_local_block((FunctionType*)(data_block), box.low, box.high, threshold);

  // char filename[128];
  // sprintf(filename, "outblock_%d.raw", task->index_point.point_data[0]);
  // std::ofstream outfile (filename,std::ofstream::binary);
  // outfile.write(pay.buffer(), pay.size());
  // outfile.close();

  // printf("volume in %d size %d\n", task->index_point.point_data[0], pay.size());
  assert(elem_rect_in.volume() >= pay.size()/BYTES_PER_POINT);

  Controller::bufferToRegion(pay.buffer(), pay.size()/BYTES_PER_POINT, elem_rect_in, regions[0]);//&ctx, runtime);
  //runtime->unmap_region(ctx,regions[0]);

  // char offname[128];
  //   sprintf(offname,"read_%d.raw", RegionsIndexType(elem_rect_in.lo));
  //   std::ofstream outresfile(offname,std::ofstream::binary);

  //    outresfile.write(reinterpret_cast<char*>(pay.buffer()),pay.size());

  //    outresfile.close();

  delete [] pay.buffer();

  return task->regions[0].region;
}

#endif


int main(int argc, char* argv[])
{
#if defined(NOSYNC)
  std::cout.sync_with_stdio(false);
#endif

  if (argc < 9) {
    fprintf(stderr,"Usage: %s -f <input_data> -d <Xdim> <Ydim> <Zdim> \
            -p <dx> <dy> <dz> -m <fanin> -t <threshold>\n", argv[0]);
    return 0;
  }

  this_argc=argc;
  this_argv=argv;

  /*
  int num_entries = 4;
  gasnet_nodeinfo_t ginfo;
  int rank = gasnet_getNodeInfo(&ginfo, num_entries) ;//gasnet_mynode();
  printf("I'm node %d argc %d\n", ginfo.supernode, argc);
  */
  
  uint32_t data_size[3]={0,0,0};        // {x_size, y_size, z_size}
  uint32_t block_decomp[3]={0,0,0};     // block decomposition
  int nblocks;                  // number of blocks per input task
  int share_face = 1;           // share a face among the blocks
  uint32_t valence = 2;
  FunctionType threshold = (FunctionType)(-1)*FLT_MAX;
  char* dataset = "";
  
  for (int i = 1; i < argc; i++){
    if (!strcmp(argv[i],"-d")){
      data_size[0] = atoi(argv[++i]);                                                                                                              
      data_size[1] = atoi(argv[++i]);                                                                                                              
      data_size[2] = atoi(argv[++i]); 
    }
    if (!strcmp(argv[i],"-p")){
      block_decomp[0] = atoi(argv[++i]);                                                                                                           
      block_decomp[1] = atoi(argv[++i]);                                                                                                           
      block_decomp[2] = atoi(argv[++i]);
    }
    if (!strcmp(argv[i],"-m"))
      valence = atoi(argv[++i]);
    if (!strcmp(argv[i],"-t"))
      threshold = atof(argv[++i]);
    if (!strcmp(argv[i],"-f"))
      dataset = argv[++i];
  }

#ifdef VISUS_IDXIO
  printf("Using ViSUS IDXIO\n");

  Visus::SetCommandLine(argc, (const char**)argv);
  Visus::IdxModule::attach();
  Visus::DoAtExit do_at_exit([]{
       Visus::IdxModule::detach();
  });
#endif

#ifdef USE_GASNET
  int rank = gasnet_mynode();
#else
  int rank = 0;
#endif

  if(rank == -1)
    std::cout << "run config " << block_decomp[0] << block_decomp[1] << block_decomp[2] << " nprocs " << block_decomp[0]* block_decomp[1]* block_decomp[2] << std::endl ;
  
  TaskId num_inputs = block_decomp[0]*block_decomp[1]*block_decomp[2];

  int nclusters = num_inputs/valence;

  //BabelFlow::SuperTask* st = graph.getSuperLocal();//graph.clusterize(nclusters);
  
  KWayMerge graph(block_decomp, valence);
#if USE_SPMD_CONTROLLER
  KWayTaskMap task_map(num_inputs, &graph);
#else
  KWayTaskMap task_map(1, &graph);
#endif

  std::map<TaskId,Payload> initial_input;
  MergeTree::setDimension(data_size);

  /*
  FILE* output = fopen("graph.dot","w");
  // graph.output_graph(1, &task_map, output);
  KWayTaskMap task_map_debug(1, &graph);
  std::vector<BabelFlow::Task> tasks_deb = graph.localGraph(0,&task_map_debug);
  output_full_graph(output, tasks_deb);
  fclose(output);  
  */

  Controller* master = Controller::getInstance();
  master->registerInputInitialization(&input_initialization_nodata);
  // Register the callbacks
#if USE_SPMD_CONTROLLER
  master->initialize(&graph, &task_map, argc, argv);
#else
  master->initialize(graph, &task_map, argc, argv);
#endif

  master->registerCallback(1, local_compute);
  master->registerCallback(2, join);
  master->registerCallback(3, local_correction);
  master->registerCallback(4, write_results);

#if USE_SPMD_CONTROLLER
  Runtime::register_legion_task<bool, load_task>(LOAD_TASK_ID,
                      Processor::LOC_PROC, true/*single*/, false/*index*/,
                      AUTO_GENERATE_ID, TaskConfigOptions(false/*leaf*/), "load_task");
#else
  HighLevelRuntime::register_legion_task<LogicalRegion, load_task>(LOAD_TASK_ID,
          Processor::LOC_PROC, true/*single*/, false/*index*/,
          AUTO_GENERATE_ID, TaskConfigOptions(true/*leaf*/), "load_task");
#endif

  master->run(initial_input);

  double finish = Realm::Clock::current_time_in_microseconds()/1000000.0f ;

#if DETAILED_TIMING
  std::cout << std::fixed << rank << ": proc total time = " << finish << std::endl;
#endif
  return 0;
  
 }
