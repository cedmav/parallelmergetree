
--------------------------------------
Overview
--------------------------------------

This repositories contains a multi-runtime implementation of a distributed merge tree segmentation algorithm.
The implemenation relies on the framework BabelFlow, which allows to execute the algorithm on different runtime systems.

--------------------------------------
Build
--------------------------------------

This project depends on BabelFlow, you can download and build the framework using your preferred runtime (from here https://github.com/sci-visus/BabelFlow).

After building and installing BabelFlow, this project can be build using CMake as following:

```
mkdir build
cd build
cmake .. -DBabelFlow_DIR=/path/to/babelflow/install/lib/cmake/BabelFlow
make
```

CMake will automatically recognize the runtime used by the BabelFlow build and require additional variables to define according to the runtime selected (e.g., same as used by your BabelFlow build).

--------------------------------------
Execution
--------------------------------------

Depending on the selected runtime you will find an executable in your build directory that you can use as following:

`./[runtime_name]/pmt_[runtime_name] -f <input_data> -d <Xdim> <Ydim> <Zdim> -p <dx> <dy> <dz> -m <fanin> -t <threshold>`

- *input_data* is in RAW format for MPI and IDX format (using https://github.com/sci-visus/OpenVisus) for Legion and Charm++
- *dx, dy and dx* are the number of partitions used to decompose the dataset over the 3 axis.
- *fanin* is the number of blocks merged at each join operation (e.g., 8)
- *threshold* is the threshold to ignore features under a certain value, if not provided threshold=MIN_FLOAT

--------------------------------------
Install via spack (library only)
--------------------------------------

We are using (uberenv)[https://github.com/LLNL/uberenv] to build with spack.
You can simply run the *uberenv* script as follow:

```
./scripts/uberenv/uberenv.py
```

