/*
 * Controller.h
 *
 *  Created on: May 30, 2016
 *      Author: spetruzza
 */

#ifndef READ_BLOCK_H
#define READ_BLOCK_H

#include "TypeDefinitions.h"

extern "C" {

char* read_block(char* filename, GlobalIndexType* from, GlobalIndexType* to);

}
#endif
